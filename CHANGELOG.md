## 2.0.3-rc.1
1.修复长按图片事件失效

## 2.0.3-rc.0
1.修复不兼容API9问题

## v2.0.2
1. ArkTs语法适配
2. 新增OnImageEventListener, OnLongPressListener, OnDoubleTapListener, OnSingleTapListener, OnStateChangedListener接口导出
3. 新增Config枚举导出

## v2.0.1

1. 修复SubsamplingScaleImageView引用文件名的大小写

## v2.0.0

1. 包管理工具由npm切换成ohpm

2. 适配DevEco Studio 版本：3.1 Beta2（3.1.0.400）

3. 适配OpenHarmony SDK版本:API9（3.2.11.9）

## v1.0.2

api8升级到api9

## v1.0.0

1. 支持加载网络图片或Resource或PixelMap图片
2. 支持缩放功能
3. 支持平移功能