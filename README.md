# subsampling-scale-image-view

## 简介

>深度缩放视图，图像显示，手势平移缩放双击等

## 效果图（旋转、缩放、平移）
![img](screenshot/rotate.png)![img](screenshot/scale.png)![img](screenshot/translate.png)



## 下载安装
```shell
ohpm install @ohos/subsampling-scale-image-view 
```

OpenHarmony ohpm 环境配置等更多内容，请参考[如何安装 OpenHarmony ohpm 包](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_har_usage.md)

## 使用说明

### 生成SubsamplingScaleImageView

   ```
   import {SubsamplingScaleImageView} from '@ohos/subsampling-scale-image-view';
   ...
   //创建model对象
   @State model: SubsamplingScaleImageView.Model  = new SubsamplingScaleImageView.Model()
   ...
   build() {
     Stack({ alignContent: Alignment.Bottom }) {
      //使用SubsamplingScaleImageView组件
      SubsamplingScaleImageView({ model: this.model })
      Column({ space: 5 }) {
        Swiper(this.swiperController) {
          Row({ space: 5 }) {
            Text('This image is 7,800 x 6,240 pixels. On most devices it will be subsampled, and higher quality tiles are loaded as you zoom in.')
              .width('100%')
              .height(60)
              .layoutWeight(1)
              .fontColor(0xffffff)
              .textAlign(TextAlign.Center)
              .fontSize(16)
            Image($r('app.media.next'))
              .width(30)
              .height(30)
              .margin({ top: 6, left: 10 })
              .onClick((event: ClickEvent) => {
                this.index = 1;

              })
          }.width('100%').height(60).backgroundColor(0x3d3d3d)

          Row({ space: 5 }) {
            Image($r('app.media.previous')).width(30).height(30).margin({ top: 6 }).onClick((event: ClickEvent) => {
              this.index = 0;
            })
            Text('This image has been rotated 90 degrees. Tap the button to rotate it. EXIF rotation is supported for external files.')
              .width('100%')
              .height(60)
              .layoutWeight(1)
              .fontColor(0xffffff)
              .textAlign(TextAlign.Center)
              .fontSize(16)
            Image($r('app.media.rotate'))
              .width(30)
              .height(30)
              .margin({ top: 6, left: 10, right: 20 })
              .onClick((event: ClickEvent) => {
                this.mRotate += 90;
                this.model.setOrientation(this.mRotate)
              })
          }.width('100%').height(60).backgroundColor(0x3d3d3d)

        }
        .index(this.index)
        .autoPlay(false)
        .indicator(false) // 默认开启指示点
        .loop(false) // 默认开启循环播放
        .duration(50)
        .vertical(false) // 默认横向切换
        .itemSpace(0)
        .onChange((index: number) => {
          if (index == 1) {
            
            this.model.setImage($r('app.media.swissroad'));
          }
        })
      }.height(60).backgroundColor(0x3d3d3d).align(Alignment.Bottom)
    }
   }
   //设置图片源
   aboutToAppear() {
    this.model.setImage($r('app.media.card'));
   }
   ...
   ```
## 接口说明

1. 设置图片资源

   ```
   public setImage(src: string | PixelMap | Resource)
   public setImage(src: string | PixelMap | Resource, previewSource: string | Resource)
   public setImage(src: string | PixelMap | Resource, state: ImageViewState)
   ```
2. 设置图片是否可缩放
   ```
   setZoomEnabled(zoomEnabled: boolean)
   ```
3. 设置图片是否可平移
   ```
   public setPanEnabled(panEnabled: boolean)
   ```
4. 设置图片最大缩放比
   ```
   public setMaxScale(maxScale: number)
   ```
5. 设置图片旋转角度
   ```
   public setOrientation(degrees: number)
   ```
6. 获取图片高宽
   ```
   public getSWidth()  
   public getSHeight()
   ```
7. 单击监听器
   ```
   public setSingleTapListener(listener: OnSingleTapListener)
   ```
8. 长按监听器  
   ```
   public setLongPressListener(listener: OnLongPressListener)
   ```
9. 双击监听器
   ```
   public setDoubleTapListener(listener: OnDoubleTapListener)
   ```

## 约束与限制

在下述版本验证通过：
- DevEco Studio 版本： 4.1 Canary(4.1.3.520)-OpenHarmony SDK:API11 (4.1.0.63)
- DevEco Studio 版本： 4.1 Canary(4.1.3.317)-OpenHarmony SDK:API11 (4.1.0.36)

## 目录结构

````
|---- subsampling-scale-image-view 
    |---- entry # 示例代码文件夹      
    |---- library  
    |     |---- src
    |          |---- main
    |                |---- ets          
    |                      |---- components  # 库文件夹               
    |                           |---- SubsamplingScaleImageView.ets  # 自定义组件
    |                           |---- ImageViewState.ets  # 组件状态数据封装类  
    |     |---- index.ets  # 对外接口              
    |     |---- README.md  # 安装使用方法
````

## 贡献代码
使用过程中发现任何问题都可以提 [Issue](https://gitee.com/openharmony-sig/subsampling-scale-image-view/issues) 给我们，当然，我们也非常欢迎你给我们发 [PR](https://gitee.com/openharmony-sig/subsampling-scale-image-view/pulls) 。

## 开源协议
本项目基于 [Apache License 2.0](https://gitee.com/openharmony-sig/subsampling-scale-image-view/blob/master/LICENSE) ，请自由地享受和参与开源。

  
